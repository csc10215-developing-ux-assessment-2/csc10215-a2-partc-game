﻿/* Title: LoadScene script
 * Author/s: Zac Sheather & Jessica McKendry
 * Purpose: To provide a function for navigating between scenes (i.e., different screens)
 * Last modified: 12/02/2020
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LoadScene : MonoBehaviour
{
    private int sceneIndex;

    // Start is called when the scene first loads
    void Start()
    {
        SceneManager.activeSceneChanged += ChangedActiveScene;
    }

    // SceneLoader is called by the navigation buttons
    // Author: Zac Sheather
    public void SceneLoader(int index)
    {
        //Debug.Log("sceneBuildIndex to load: " + index);
        sceneIndex = index;
        StartCoroutine("SceneLoaderCoroutine");
    }

    // SceneLoaderCoroutine allows a short delay so button click sounds aren't cut off
    // Author: Jessica McKendry
    IEnumerator SceneLoaderCoroutine()
    {
        //get click sound audio clip
        AudioClip clickSound = Resources.Load<AudioClip>("click");

        //wait for length of clip
        yield return new WaitForSeconds(clickSound.length);

        //then load new scene
        SceneManager.LoadScene(sceneIndex);
    }

    // ChangedActiveScene is called when the game switches scenes
    // Author: Jessica McKendry
    private void ChangedActiveScene(Scene current, Scene next)
    {
        //***below is for debugging***        
        string currentName = current.name;  //current scene        
        string nextSceneName = next.name;   //next scene

        // if scene has been removed
        if (currentName == null)
        {
            currentName = "Replaced";
        }

        //Debug.Log("Scenes: " + currentName + ", " + nextSceneName);  for debugging
        //***end Debug***

        //set soundButton appearance for all except game level screens
        if (!nextSceneName.Contains("Level"))
        {
            AudioManager.instance.SetSoundButton();
            //make sure the game doesn't try to mark it as false
            if(GameManager.instance != null)
            {
                GameManager.instance.gameOver = true;
            }
        }
        else        //trigger scene change in game manager
        {
            int levelNumber = (int)Char.GetNumericValue(nextSceneName[16]);
            //print("Level number: " + levelNumber);    //for debugging
            GameManager.instance.LoadNewLevel(levelNumber);
        }
    }
}
