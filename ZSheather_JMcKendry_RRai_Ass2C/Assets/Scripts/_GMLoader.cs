﻿/* Title: _GMLoader script
 * Author: Jessica McKendry
 * Purpose: Loads the GameManager from the prefab
 * Last modified: 05/02/2020
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _GMLoader : MonoBehaviour
{
    public GameObject gameManager;
    
    void Awake()
    {
        if(GameManager.instance == null)
        {
            Instantiate(gameManager);
        }
    }
}
