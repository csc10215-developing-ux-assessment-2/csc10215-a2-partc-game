﻿/* Title: Spikes script
 * Author: Jessica McKendry
 * Purpose: Provides functions for the Spikes object
 * Last modified: 11/02/2020
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {

    }

    //check for colliders touching
    void OnCollisionEnter2D(Collision2D collision)
    {
        //test if the player is standing on spikes
        LayerMask layerMask = LayerMask.GetMask("Player");
        float originOffset = 0f;
        float distance = 0.5f;
        Vector2 direction = Vector2.up;

        Vector2 position = new Vector2(transform.position.x, transform.position.y + originOffset);

        //Debug.DrawRay(position, direction, Color.green);    for debugging
        RaycastHit2D hit = Physics2D.Raycast(position, direction, distance, layerMask);

        //if the Raycast detects the player
        if (hit.collider != null)
        {
            Player player = hit.collider.gameObject.GetComponent<Player>();
            if (player != null)
            {
                GameManager.instance.AttackPlayer(player);
            }
        }
    }
}
