﻿/* Title: GameOver script
 * Author: Jessica McKendry
 * Purpose: To properly display scores & play music on reaching the game over screen
 * Last modified: 14/02/2020
 */
 
 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    //variables
    private int personalBest = 0;     //this is stored in the player prefs file
    private bool endMusicPlayed = false;

    //canvas elements
    public Text yourScoreTextbox;
    public Text personalBestTextbox;
    public Text pageTitle;
    public GameObject personalBestAlertPanel;
    
    // Start is called before the first frame update
    void Start()
    {
        //if there is a previously existing 'personal best score'
        if (PlayerPrefs.HasKey("personalBest"))
        {
            //set it to that for now
            personalBest = PlayerPrefs.GetInt("personalBest");
        }

        checkForNewPersonalBest(); 

        //display current game & personal best scores
        yourScoreTextbox.text = GameManager.instance.stats.CurrentScore.ToString();
        personalBestTextbox.text = personalBest.ToString();
    }

    void Update()
    {
        if (!endMusicPlayed)
        {
            //if the player has won the game
            if (GameManager.instance.stats.PlayerWon)
            {
                //play sound & display game won text
                AudioManager.instance.PlaySound("gameWon");
                pageTitle.text = "Well done, you won the game!";
            }
            else
            {
                //play sound & display game lost text
                AudioManager.instance.PlaySound("gameLost");
                pageTitle.text = "You didn't win the game. Too bad!";
            }
            endMusicPlayed = true;
        }
    }

    //checkForNewPersonalBest determines if the player has made a new personal best score
    void checkForNewPersonalBest()
    {
        //if the player has made a new personal best score in this game
        if (GameManager.instance.stats.PersonalBest > personalBest)
        {
            //print("You have a new personal best score"); for debugging

            //update personalBest to the new personal best score
            personalBest = GameManager.instance.stats.PersonalBest;
            //set alert panel to active
            personalBestAlertPanel.SetActive(true);
        }
    }
}
