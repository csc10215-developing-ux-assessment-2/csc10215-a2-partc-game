﻿/* Title: CameraController script
 * Author: Jessica McKendry
 * Purpose: Sets the camera to follow the player
 * Last Modified: 07/02/2020
 */
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;   //for getting the player's position

    public float smoothSpeed = 0.125f;  //movement smoothing
    private Vector3 offset;     //player to camera offset
    
    // Start is called before the first frame update
    void Start()
    {
        //find initial position differences between player and camera
        offset = transform.position - player.transform.position;        
    }

    // LateUpdate is called after all Update functions and should be used for follow cameras
    void LateUpdate()
    {
        if(player == null)
        {
            FindPlayer();
            return;
        }

        Vector3 desiredPosition = player.transform.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
    
        //move camera to player's position, accounting for offset
        transform.position = smoothedPosition;
    }

    // FindPlayer is called when the player has been removed from the game
    void FindPlayer()
    {
        GameObject search = GameObject.FindGameObjectWithTag("Player");
        if(search != null)
        {
            player = search;
        }
    }
}
