﻿/* Title: HighScoresTable script
 * Author: Jessica McKendry
 * Purpose: Loads high scores and checks player data for high score/new personal best
 * Last modified: 14/02/2020
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

public class HighScoresTable : MonoBehaviour
{
    private HighScoresList highScoreEntryList;      //collection of high scores
    public Transform entryContainer;        //holds on-screen entries
    public Transform entryTemplate;         //template for on-screen entry displays
    private static float templateHeight = 35f;      //height of on-screen entries
    private List<Transform> highScoreTransformList;     //on-screen high score entries created

    private string filePath;    //file path of XML high scores data
    private static int maxNumberOfScores = 5;   //only top 5 scores are displayed

    public UsernameWindow usernameWindow;       //window for player to input username
    public Button submitButton;     //submit button on usernameWindow
    private UnityAction getUsername;

    private bool waitForInput;      //used to wait for username input
    private bool readyToDisplayScores;      //program is ready to display high scores

    //variables to hold saved player data
    int playerBestScore = 0;
    string username = "";

    // Awake
    void Awake()
    {
        //hide default table entry template
        entryTemplate.gameObject.SetActive(false);

        filePath = Application.dataPath + "/DataFiles/highScores.xml";
        waitForInput = false;
        readyToDisplayScores = true;
    }

    void Start()
    {
        getUsername += GetNameInput;
        GetHighScoresFromFile();
        TestForPlayerHighScore();
    }

    // Update is called once per frame
    void Update()
    {
        //if waiting for username input
        if (waitForInput)
        {
            if (submitButton == null)
            {
                submitButton = GetComponent<Button>();
            }
            else
            {
                submitButton.onClick.AddListener(getUsername);
            }
        }        

        //if not currently waiting for input & player's score has been checked
        if (!waitForInput && readyToDisplayScores)
        {
            //print("ready to display scores");     for debugging
            DisplayHighScores();
        }
    }

    void GetHighScoresFromFile()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(HighScoresList));

        if (File.Exists(filePath))
        {
            //print("File exists");
            FileStream readStream = new FileStream(filePath,FileMode.Open);     //open file
            highScoreEntryList = serializer.Deserialize(readStream) as HighScoresList;
            readStream.Close();     //close file
        }
        else
        {
            print("File does not exist: " + filePath);
        }
    }

    void WriteHighScoresToFile()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(HighScoresList));
        FileStream writeStream = new FileStream(filePath, FileMode.Create);
        serializer.Serialize(writeStream, highScoreEntryList);
        writeStream.Close();
    }

    //TestForPlayerHighScore will check to see if player ranks in the top 5 scores
    void TestForPlayerHighScore()
    {
        //if the user has a personal best high score
        if (PlayerPrefs.HasKey("personalBest"))
        {
            playerBestScore = PlayerPrefs.GetInt("personalBest");

            //if the player has made a new personal best score in this game
            if (GameManager.instance != null)
            {
                if (GameManager.instance.stats.PersonalBest > playerBestScore)
                {
                    playerBestScore = GameManager.instance.stats.PersonalBest;
                    PlayerPrefs.SetInt("personalBest", playerBestScore);

                    //loop through current high scores
                    foreach (HighScoreEntry highScore in highScoreEntryList.HighScores)
                    {
                        //if the player has a score greater than the bottom high score
                        if (playerBestScore >= (highScore.score + 1))
                        {
                            //print("player beat someone");     for debugging
                            //prompt player for username
                            usernameWindow.Show();
                            waitForInput = true;
                            readyToDisplayScores = false;
                            return;
                        }
                    }
                }
            }
        }
        else
        {
            //no recorded personal best but player has made a personal best score
            if (GameManager.instance != null)
            {
                playerBestScore = GameManager.instance.stats.PersonalBest;
                PlayerPrefs.SetInt("personalBest", playerBestScore);
            }
        }
    }

    void GetNameInput()
    {
        username = usernameWindow.GetName();
        waitForInput = false;
        readyToDisplayScores = true;
    }

    void DisplayHighScores()
    {
        //if the player has achieved a high score and assigned a username
        if (username != "")
        {
            //add to high scores list
            HighScoreEntry newScore = new HighScoreEntry { name = username, score = playerBestScore };
            highScoreEntryList.HighScores.Add(newScore);
            //print(newScore);     //for debugging
        }

        //display on-screen below leaderboard list
        GameObject.Find("personalBestText").GetComponent<Text>().text = playerBestScore.ToString();

        //order list by highest score
        highScoreEntryList.HighScores.Sort((x, y) => y.score.CompareTo(x.score));

        //remove excess entries
        if (highScoreEntryList.HighScores.Count > maxNumberOfScores)
        {
            int amountToRemove = highScoreEntryList.HighScores.Count - maxNumberOfScores;
            highScoreEntryList.HighScores.RemoveRange(maxNumberOfScores, amountToRemove);
        }

        //now create and add text list to leaderboard screen
        highScoreTransformList = new List<Transform>();
        foreach (HighScoreEntry highScore in highScoreEntryList.HighScores)
        {
            //print(highScore.name + ": " + highScore.score);       for debugging
            CreateHighScoreEntry(highScore, highScoreTransformList);
        }

        //write to file
        WriteHighScoresToFile();
        readyToDisplayScores = false;   //don't continue to reload scores
    }

    void CreateHighScoreEntry(HighScoreEntry highScoreEntry, List<Transform> transformList)
    {
        int position = transformList.Count + 1;
        int score = highScoreEntry.score;
        string name = highScoreEntry.name;

        //create score entry based on hidden entry template
        Transform entryTransform = Instantiate(entryTemplate, entryContainer);
        RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();

        //position new entry
        entryRectTransform.anchoredPosition = new Vector2(0, -(templateHeight * transformList.Count));
        entryTransform.gameObject.SetActive(true);  //display new entry

        //set entry contents
        entryTransform.Find("posText").GetComponent<Text>().text = position.ToString();
        entryTransform.Find("scoreText").GetComponent<Text>().text = score.ToString();
        entryTransform.Find("nameText").GetComponent<Text>().text = name;

        transformList.Add(entryTransform);
    }
}

[System.Serializable]
public class HighScoreEntry
{
    [XmlAttribute("username")]
    public string name;
    public int score;
}

[System.Serializable]
[XmlRoot("HighScoresContainer")]
public class HighScoresList
{
    [XmlArray("HighScores"), XmlArrayItem("HighScore")]
    public List<HighScoreEntry> HighScores = new List<HighScoreEntry>();
}