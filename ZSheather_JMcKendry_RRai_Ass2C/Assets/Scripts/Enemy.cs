﻿/*Title: Enemy properties
 * Author: Rocky
 * Purpose: Provides characteristics to the enemy on the game
 * Last Edited: 12/02/2020
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{


	public float speed = 0.50F;		//the speed of enemy
	public bool movingRight = true;
	public float stepsCounter;      //the number of steps to flip the enemy's direction.
	public float maxSteps = 100;    //the length of distance for the enemy to walk.
	public bool dieStatus = false;  //the living status of enemy.
    public int points = 20;     //the number of points the enemy is worth
    public bool facingRight = true;     //holds if enemy is facing right

	private Animator anim;

    void Start()
	{
		anim = GetComponent<Animator>();
	}

	//-------This update function is called every frame of the game-------
	void Update()
	{	
		//enemy only moves when it is alive.
		if (dieStatus == false)
		{
			//-----This will move the enemy------
			transform.Translate(Vector2.right * speed * Time.deltaTime * 2);
			stepsCounter++;

			//to flip the enemy after certain footsteps.
			if (stepsCounter == maxSteps)
			{
				Flip();
				stepsCounter = 0;
			}
		} 
	}

	//----------This will flip the enemy horizontally-------------------------------
	private void Flip()
	{
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
		speed *= -1;
        facingRight = !facingRight;
	}

	//-----------This will attack the player when in near----------------------------
	public void OnCollisionEnter2D(Collision2D other)
	{
		//only does thing if the enemy is alive
		if (dieStatus == false)
        {
            if (other.gameObject.tag.Equals("Player"))
			{
                /******** 
                 * Hi Rocky, I fixed the raycast to make
                 * the enemy only attack when they're facing the player.
                 * Hope that's ok!
                 * - Jess
                 * *********/
                //use RayCast to check if enemy is facing player
                LayerMask layerMask = LayerMask.GetMask("Player");
                float originOffset = 0.35f;
                float distance = 1.0f;
                Vector2 direction = Vector2.right;

                //adjust for facing left
                if (!facingRight)
                {
                    originOffset = -0.35f;
                    direction = Vector2.left;
                }

                Vector2 position = new Vector2(transform.position.x + originOffset, transform.position.y);

                //Debug.DrawRay(position, direction, Color.green);    display RayCast for debugging
                RaycastHit2D hit = Physics2D.Raycast(position, direction, distance, layerMask);

                //if the Raycast detects the player
                if (hit.collider != null)
                {
					anim.SetTrigger("isCollided");
                    print("Enemy attacks player");    //for debugging in the unity console
                    GameManager.instance.AttackPlayer(other.gameObject.GetComponent<Player>());
                }

                /*
				timeLeft--;
				if (timeLeft < 0)	//attacks player after timeLeft is less than 0
				{
                    //****if enemy is facing player
                    print("Enemy can now attack player");    //for debugging in the unity console
					timeLeft = 5;   //resets the time again for next collision					
					//rest code still remaining
					//
					//
				}*/
			}
		}
	}

	//----------This will make the enemy die when player attacks-------------------------
	public void Die()
	{
        //AudioManagerScript.PlaySound("enemyDeath");
 		AudioManager.instance.PlaySound("enemyDeath");

		anim.SetTrigger("isDie");
		dieStatus = true;
		print("enemy got hit");
	}
}
