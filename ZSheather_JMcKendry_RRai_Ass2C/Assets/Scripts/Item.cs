﻿/* Title: Item script
 * Author: Zachary Sheather
 * Purpose: Provides functions for collecting the item pickups
 * Last modified: 11/02/2020
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            print("player touched item");
            GameManager.instance.AddScore(10);
            Destroy(gameObject);
        }
    }
}
