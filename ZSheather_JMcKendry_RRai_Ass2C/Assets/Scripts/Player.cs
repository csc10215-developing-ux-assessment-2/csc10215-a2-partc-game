﻿/* Title: Player script
 * Author: Jessica McKendry
 * Purpose: Provides the player stats and other information stored by the player class
 * Last modified: 07/02/2020
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour
{
    private PlayerAnimation playerAnimation;    //calls in PlayerAnimation script
    public bool isDying;        //dying state of player

    //HUD element
    private TextMeshProUGUI playerLivesTextbox;

    void Awake()
    {
        gameObject.name = "Player";     //fixes '(Clone)' in name tag when prefab instantiates

        //fix Z rotation issue
        Rigidbody2D rigidbody = gameObject.GetComponent<Rigidbody2D>();
        rigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;

        playerAnimation = GetComponent<PlayerAnimation>();        //get player animation script
        playerLivesTextbox = GameObject.Find("PlayerLivesText").GetComponent<TextMeshProUGUI>();   //get player lives textbox
        isDying = false;        //dying state of player
    }

    // Start is called before the first frame
    void Start()
    {
        playerLivesTextbox.text = GameManager.instance.stats.Lives.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Die is called when the player loses a life
    public void Die()
    {
        isDying = true;
        GameManager.instance.stats.Lives--;
        //update lives in HUD
        playerLivesTextbox.text = GameManager.instance.stats.Lives.ToString();
        AudioManager.instance.PlaySound("playerGotHit");    //play sound
        playerAnimation.Die(true); //dying animation
    }

    //ExtraLife is called when the player score is a modulus of 100
    public void ExtraLife()
    {
        print("You get an extra life!");
        GameManager.instance.stats.Lives++;
        playerLivesTextbox.text = GameManager.instance.stats.Lives.ToString();
    }
}
