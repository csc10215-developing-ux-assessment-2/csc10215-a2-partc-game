﻿/* Title: GameOptionsMenu script
 * Author: Jessica McKendry
 * Purpose: Displays/hides the game options menu
 * Last Modified: 01/02/2020
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOptionsMenu : MonoBehaviour
{
    public static bool gameIsPaused = false;

    public GameObject gameOptionsUI;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))   //check for escape button press
        {
            if (gameIsPaused)
            {
                ResumeGame();
            }
            else
            {
                PauseGame();
            }
        }
    }

    public void ResumeGame()
    {
        gameOptionsUI.SetActive(false);  //hide game options menu
        Time.timeScale = 1f;   //unpauses game
        gameIsPaused = false;
    }

    public void PauseGame()
    {
        gameOptionsUI.SetActive(true);  //displays game options menu
        Time.timeScale = 0f;   //pauses game
        gameIsPaused = true;

        //set soundButton appearance
        AudioManager.instance.SetSoundButton();
    }
}
