﻿/* Title: PlayerAnimation script
 * Author: Jessica McKendry
 * Purpose: Provides functions for animating the player character
 * Last modified: 04/02/2020
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    public Animator animator;

    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void Walk(float walk)
    {
        //sets the animation parameter 'walk'
        animator.SetFloat("walk",Mathf.Abs(walk));
    }

    public void Jump(bool isJumping)
    {
        //sets the animation parameter 'isJumping'
        animator.SetBool("isJumping", isJumping);
    }

    public void Climb(float climb)
    {
        //sets the animation parameter 'climb'
        animator.SetFloat("climb", Mathf.Abs(climb));
    }

    public void Attack(bool isAttacking)
    {
        //sets the animation parameter 'isAttacking'
        animator.SetBool("isAttacking", isAttacking);
    }

    public void Die(bool isDying)
    {
        //sets the animation parameter 'isDying'
        animator.SetBool("isDying", isDying);
    }
}
