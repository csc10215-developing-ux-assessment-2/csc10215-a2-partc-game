﻿/* Title: TreasureChest script
 * Author: Jessica McKendry
 * Purpose: Provides functions for triggering 'Game Won'
 * Last modified: 13/02/2020
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureChest : MonoBehaviour
{
    private Animator anim;

    void Awake()
    {
        anim = GetComponent<Animator>();
        anim.SetBool("playerIsTouching", false);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            print("player collected chest");
            anim.SetBool("playerIsTouching", true);
        }
    }

    void GameWon()
    {
        GameManager.instance.stats.PlayerWon = true;
        GameManager.instance.GameOver();
    }

    void PlayChestOpenSound()
    {
        AudioManager.instance.PlaySound("chestOpen");
    }
}
