﻿/* Title: UsernameWindow script
 * Author: Jessica McKendry
 * Purpose: Gets player username if they've made a new high score
 * Last modified: 14/02/2020
 */
 
using System;
using UnityEngine;
using UnityEngine.UI;

public class UsernameWindow : MonoBehaviour
{
    private string username;
    private Button submitButton;
    private InputField inputField;

    public void Show()
    {
        gameObject.SetActive(true);
        submitButton = transform.Find("SubmitButton").GetComponent<Button>();
        inputField = transform.Find("NameInputField").GetComponent<InputField>();
    }

    void Hide()
    {
        gameObject.SetActive(false);
    }

    public string GetName()
    {
        username = inputField.text;
        Hide();
        return username;
    }
}
