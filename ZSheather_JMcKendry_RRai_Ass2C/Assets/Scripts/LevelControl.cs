﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelControl : MonoBehaviour
{
    public int index;
    public GameObject sceneManager;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            sceneManager.GetComponent<LoadScene>().SceneLoader(index);
        }
    }
}
