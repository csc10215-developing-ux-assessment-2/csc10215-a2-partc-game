﻿/* Title: Background music script
 * Author: Rocky Rai
 * Purpose: Provide continuous backgound music to the game.
 * Last modified: 05/02/2020
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyAudio : MonoBehaviour
{
     void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }
}
