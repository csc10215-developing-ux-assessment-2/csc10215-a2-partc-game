﻿/* Title: AudioManager script
 * Author/s: Rocky Rai, Jessica McKendry
 * Purpose: Manages the playing of game audio & SFX clips
 * Last modified: 12/02/2020
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance = null;     //creates a publicly accessible instance

    public static AudioClip clickSound, climbLadderSound, enemyDeathSound,
        gameLostSound, gameWonSound, playerGotHitSound, playerJumpSound,
        hoverSound, attackSound, scoreSound, walkSound, chestOpenSound; // audio clips
       
    private static AudioSource audioSrc;    //audio source that plays the clip

    public Sprite soundOffSprite;
    public Sprite soundOnSprite;
    private Button soundButton;    //toggle sound button
    private bool soundOn;   //whether sound is turned on and playing or not

    //Awake is called when the object is created
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
        {
            //if not, set instance to this
            instance = this;
        }
        else if (instance != this)      //if instance already exists and it's not this
        {
            Destroy(gameObject);    //destroy this (there can only be one instance of SoundManager)
        }

        //make sure a newly loaded scene doesn't destroy the SoundManager
        DontDestroyOnLoad(gameObject);
        InitialiseSound();
    }

    //Start is called when the scene first loads
    void Start()
    {
        clickSound = Resources.Load<AudioClip>("click");
        hoverSound = Resources.Load<AudioClip>("hover");
        scoreSound = Resources.Load<AudioClip>("score");
        climbLadderSound = Resources.Load<AudioClip>("climbLadder");
        walkSound = Resources.Load<AudioClip>("walk");
        enemyDeathSound = Resources.Load<AudioClip>("enemyDeath");
        gameLostSound = Resources.Load<AudioClip>("gameLost");
        gameWonSound = Resources.Load<AudioClip>("gameWon");
        playerGotHitSound = Resources.Load<AudioClip>("playerGotHit");
        attackSound = Resources.Load<AudioClip>("attack");
        playerJumpSound = Resources.Load<AudioClip>("playerJump");
        chestOpenSound = Resources.Load<AudioClip>("chestOpen");

        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PlaySound(string clip)
    {
        switch (clip)
        {
            case "click":
                audioSrc.PlayOneShot(clickSound);
                break;
            case "hover":
                audioSrc.PlayOneShot(hoverSound);
                break;
            case "score":
                audioSrc.PlayOneShot(scoreSound);
                break;
            case "climbLadder":
                audioSrc.PlayOneShot(climbLadderSound);
                break;
            case "walk":
                audioSrc.PlayOneShot(walkSound);
                break;
            case "attack":
                audioSrc.PlayOneShot(attackSound);
                break;
            case "enemyDeath":
                audioSrc.PlayOneShot(enemyDeathSound);
                break;
            case "gameLost":
                audioSrc.PlayOneShot(gameLostSound);
                break;
            case "gameWon":
                audioSrc.PlayOneShot(gameWonSound);
                break;
            case "playerGotHit":
                audioSrc.PlayOneShot(playerGotHitSound);
                break;
            case "playerJump":
                audioSrc.PlayOneShot(playerJumpSound);
                break;
            case "chestOpen":
                audioSrc.PlayOneShot(chestOpenSound);
                break;
            default:
                Debug.Log("No sound matches the case '" + clip + "'");
                break;
        }
    }

    // ToggleSound is called on the 'Toggle Sound' button press
    // Author: Jessica McKendry
    public void ToggleSound()
    {
        //if sound is on & playing
        if (soundOn)
        {
            PlayerPrefs.SetFloat("volume", 0);  //switch sound off
        }
        else
        {
            PlayerPrefs.SetFloat("volume", 1.0f);  //switch sound on
        }

        //set main game audio        
        AudioListener.volume = PlayerPrefs.GetFloat("volume");

        soundOn = !soundOn;     //set to new toggled value

        SetSoundButton();   //change sound button appearance
    }

    // InitialiseSound is called on Awake()
    // Author: Jessica McKendry
    public void InitialiseSound()
    {
        //check if volume information exists in player prefs
        if (PlayerPrefs.HasKey("volume"))
        {
            //if sound is on
            if (PlayerPrefs.GetFloat("volume") > 0)
            {
                soundOn = false;    //sound is currently not on
                //print("sound is on");     for debugging
            }
            else
            {
                soundOn = true;    //sound is currently off
                //print("sound is off");     for debugging
            }
        }
        else    //no player prefs set for volume
        {
            //print("Key doesn't exist");      for debugging
            PlayerPrefs.SetFloat("volume", 0);
            soundOn = false;    //sound is currently not on
        }

        ToggleSound();
    }

    // SetSoundButton is called when the scene first loads,
    // the game options menu is displayed, or the user toggles the sound
    // Author: Jessica McKendry
    public void SetSoundButton()
    {
        string buttonText;
        Sprite buttonSprite;
        
        //if toggle sound button has been pressed
        if (EventSystem.current.currentSelectedGameObject != null)
        {
            if (EventSystem.current.currentSelectedGameObject.name == "ToggleSoundButton")
            {
                //get soundButton based on currently clicked button
                soundButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
            }
            else
            {
                //get sound button in standard way
                soundButton = GameObject.Find("ToggleSoundButton").GetComponent<Button>();
            }
        }
        else   //sound has been toggled on scene load etc
        {
            //if soundButton exists on-screen
            if (GameObject.Find("ToggleSoundButton") != null)
            {
                //get sound button in standard way
                soundButton = GameObject.Find("ToggleSoundButton").GetComponent<Button>();
            }            
        }

        //if sound is on
        if (PlayerPrefs.GetFloat("volume") > 0)
        {
            soundOn = true;
            buttonText = "Sound On";
            //set image to sound on sprite (Button_118)
            buttonSprite = soundOnSprite;
        }
        else
        {
            soundOn = false;
            buttonText = "Sound Off";
            //set image to sound off sprite (Button_120)
            buttonSprite = soundOffSprite;
        }

        if (soundButton != null)    //if sound button exists (it might not be created yet)
        {
            //update image & text
            soundButton.image.sprite = buttonSprite;
            soundButton.GetComponentInChildren<TextMeshProUGUI>().text = buttonText;
        }
    }
}
