﻿/* Title: GameManager script
 * Author: Jessica McKendry
 * Purpose: Controls the overall game functions and manages levels, score, player & enemy objects
 * Last modified: 12/02/2020
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;  //declares GameManager & its functions as public
    public bool gameOver;

    //PLAYER variables
    [System.Serializable]
    public class PlayerStats
    {
        public int Lives = 3;
        public float RespawnDelay = 2f;  //delay time in seconds before respawning after death
        public int CurrentScore;   //current in-game score
        public int PersonalBest;   //personal best score, initially loaded from player prefs
        public bool PlayerWon;     //whether player has won the game yet
    }
    public PlayerStats stats = new PlayerStats();

    //PLAYER prefab & script
    private Player player;    //calls in Player script
    public Transform playerPrefab;      //player prefab object
    public Transform playerSpawnPoint;  //respawn coordinates
    public ParticleSystem respawnParticle;     //player respawn particle effect

    //TIMER variable
    private float levelTimeLeft;    //number of seconds left to complete level

    //HUD elements
    private TextMeshProUGUI timeLeftTextbox;
    private TextMeshProUGUI currentScoreTextbox;
    private TextMeshProUGUI personalBestTextbox;
    //particle effects for HUD update effects
    public ParticleSystem hudParticle;
    private Transform addLivesParticlePosition;
    private Transform addScoreParticlePosition;
    private Transform bestScoreParticlePosition;

    // Awake is called when the game scene first starts
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
        {
            //if not, set instance to this
            instance = this;
        }
        else if (instance != this)      //if instance already exists and it's not this
        {
            Destroy(gameObject);    //destroy this (there can only be one instance of GameManager)
        }
        
        //make sure a newly loaded scene doesn't destroy the GameManager
        DontDestroyOnLoad(gameObject);
        stats.CurrentScore = 0;
        stats.PersonalBest = 0;

        LoadNewLevel(1);    //load level 1
        LoadBestScore();    //load personal best score
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(!gameOver)
        {
            LevelCountdownTimer();

            //if the player was respawned
            if (player == null)
            {
                //get Player script again
                player = GameObject.FindWithTag("Player").GetComponent<Player>();
            }
        }
    }

    // LoadBestScore is called at the start of the game
    void LoadBestScore()
    {
        //if previous best score exists for the player
        if (PlayerPrefs.HasKey("personalBest"))
        {
            //set this game's personalBest to player's saved score
            stats.PersonalBest = PlayerPrefs.GetInt("personalBest");
        }
        else
        {
            stats.PersonalBest = 0;
        }

        personalBestTextbox.text = stats.PersonalBest.ToString();
    }

    // AddScore is called every time the player destroys enemies or picks up item
    public void AddScore(int score)
    {
        //increase score
        stats.CurrentScore += score;

        //update onscreen text
        currentScoreTextbox.text = stats.CurrentScore.ToString();

        //get current transform position to display particles at
        addScoreParticlePosition = GameObject.Find("AddScoreParticlePosition").GetComponent<Transform>();
        //display particle effect
        Instantiate(hudParticle, addScoreParticlePosition.position, addScoreParticlePosition.rotation);

        //if current score has now beat the personal best score
        if (stats.CurrentScore >= stats.PersonalBest)
        {
            //update this game's personal best to current score
            stats.PersonalBest = stats.CurrentScore;
            //display new personal best score
            personalBestTextbox.text = stats.PersonalBest.ToString();

            //get current transform position to display particles at
            bestScoreParticlePosition = GameObject.Find("BestScoreParticlePosition").GetComponent<Transform>();
            //display particle effect
            Instantiate(hudParticle, bestScoreParticlePosition.position, bestScoreParticlePosition.rotation);
        }

        //if player score is a modulus of 100
        if (stats.CurrentScore > 0 && stats.CurrentScore % 100 == 0)
        {
            //get particle spawnpoints
            addLivesParticlePosition = GameObject.Find("AddLivesParticlePosition").GetComponent<Transform>();
            //display particle effect
            Instantiate(hudParticle, addLivesParticlePosition.position, addLivesParticlePosition.rotation);
            player.ExtraLife();
        }

        // Play sound when Item has been collected
        AudioManager.instance.PlaySound("score");
    }

    // KillEnemy is called when the player attacks an enemy
    public void KillEnemy(Enemy enemy)
    {       
        //if the player game object exists && enemy state isn't currently 'dying'
        if (player != null && enemy.dieStatus == false)
        {            
            //start delayed enemy die coroutine
            StartCoroutine(EnemyDieCoroutine(enemy));
        }
    }

    // AttackPlayer is called when the enemy attacks the player
    public void AttackPlayer(Player player)
    {
        //if the player exists & player state isn't currently 'dying'
        if (!player.isDying)
        {
            //start delayed player die coroutine
            StartCoroutine(PlayerDieCoroutine(player));
        }
    }

    // GameOver is called when the player has lost or won the game
    public void GameOver()
    {
        gameOver = true;
        print(gameOver);
        //if player has won the game
        if(stats.PlayerWon)
        {
            //for debugging
            print("Player has won the game!");
        }
        else
        {
            //for debugging
            print("Player has lost the game");
        }

        //go to game over page
        LoadScene loadSceneScript = GameObject.Find("SceneManager").GetComponent<LoadScene>();
        loadSceneScript.SceneLoader(5);
    }
    
    // PlayerDieCoroutine is called by AttackPlayer
    IEnumerator PlayerDieCoroutine(Player player)
    {
        player.Die();   //take one life away

        //wait till respawn delay has elapsed
        yield return new WaitForSeconds(stats.RespawnDelay);
        
        //if user life gets less than 0
        if (stats.Lives <= 0)
        {
            stats.PlayerWon = false;
            GameOver();     //GameOver
        }
        else
        {
            Destroy(player.gameObject);     //remove game object
            PlayerRespawn();   //respawn player
        }        
    }

    // EnemyDieCoroutine is called by KillEnemy
    IEnumerator EnemyDieCoroutine(Enemy enemy)
    {
        Debug.Log("Destroy enemy");
        enemy.Die();
        yield return new WaitForSeconds(2f);
        AddScore(enemy.points);
        //if enemy object exists
        if(enemy != null)
        {
            Destroy(enemy.gameObject);    //remove enemy object
        }
    }
    
    // Respawn is called from the GameManager if the player has lives left
    public void PlayerRespawn()
    {
        //create new instance of player prefab
        Instantiate(playerPrefab, playerSpawnPoint.position, playerSpawnPoint.rotation);
        //display particle effect
        Instantiate(respawnParticle, playerSpawnPoint.position, playerSpawnPoint.rotation);
    }

    void LevelCountdownTimer()
    {
        //if player has run out of time
        if (levelTimeLeft <= 0f)
        {
            stats.PlayerWon = false;
            timeLeftTextbox.text = "0:00";
            levelTimeLeft = 0f;
            GameOver();     //game over
        }
        else   //else countdown
        {
            levelTimeLeft -= Time.deltaTime;

            //display time left in proper format
            int minutes = Mathf.FloorToInt(levelTimeLeft / 60F);
            int seconds = Mathf.FloorToInt(levelTimeLeft - minutes * 60);
            timeLeftTextbox.text = string.Format("{0:0}:{1:00}", minutes, seconds);
        }
    }

    public void LoadNewLevel(int levelNumber)
    {
        //set level-specific variables
        gameOver = false;
        stats.PlayerWon = false;  //player has not yet won game

        player = GameObject.FindWithTag("Player").GetComponent<Player>();    //get Player script

        switch (levelNumber)
        {
            case 1:
                stats.Lives = 3;
                stats.CurrentScore = 0;
                levelTimeLeft = 180f;    //number of seconds left to complete level
                break;
            case 2:
                levelTimeLeft = 300f;
                break;
            case 3:
                levelTimeLeft = 480f;
                break;
            case 4:
                levelTimeLeft = 540f;
                break;
            case 5:
                levelTimeLeft = 600f;
                break;
            default:
                Debug.Log("Invalid level number!");
                break;
        }

        //load HUD elements
        timeLeftTextbox = GameObject.Find("TimeLeftText").GetComponent<TMPro.TextMeshProUGUI>();
        currentScoreTextbox = GameObject.Find("CurrentScoreText").GetComponent<TMPro.TextMeshProUGUI>();
        personalBestTextbox = GameObject.Find("BestScoreText").GetComponent<TMPro.TextMeshProUGUI>();

        currentScoreTextbox.text = stats.CurrentScore.ToString();
        personalBestTextbox.text = stats.PersonalBest.ToString();
}

    // GameWon is called when the player reaches the final treasure on the last level
    public void GameWon()
    {
        stats.PlayerWon = true;
        GameOver();     //game over
    }
}
