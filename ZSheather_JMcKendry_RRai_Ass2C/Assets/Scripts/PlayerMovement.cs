﻿/* Title: PlayerMovement script
 * Author: Jessica McKendry
 * Purpose: Provides functions for moving & controlling the Player character
 * Last modified: 13/02/2020
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float walkSpeed = 5F;     //player walk speed
    public float climbSpeed = 5F;      //player ladder climbing speed
    public float jumpForce = 12F;      //force of the jump
    public bool hasLanded;      //if the player has landed
    public bool canClimb = false;       //if the player is on or near ladder
    public bool ladderBelow = false;    //if their is a ladder under the player
    public bool isClimbing = false;     //if the player is currently climbing
    public bool isAttacking = false;    //if the player is currently attacking

    private bool facingRight = true;    //direction that player sprite is facing
    private float gravityScaleAtStart;

    private Rigidbody2D playerBody;
    private PlayerAnimation playerAnimation;    //calls in PlayerAnimation script
    private PlatformEffector2D platformEffector;    //for moving through platform

    void Awake()
    {
        playerBody = GetComponent<Rigidbody2D>();
        playerAnimation = GetComponent<PlayerAnimation>();
        gravityScaleAtStart = playerBody.gravityScale;
        platformEffector = GameObject.FindWithTag("Platform").GetComponent<PlatformEffector2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Walk(); //default mode, will test for idle
        CheckLadderBelow(); //checks for ladders

        //player is climbing or presses up/down
        if (isClimbing || (canClimb && Input.GetButtonDown("Vertical")))
        {
            //if player is trying to go down when there's no ladder
            if(Input.GetAxisRaw("Vertical") < 0 && !ladderBelow)
            {
                StopClimbing();
            }
            else    //else, start climbing
            {
                Climb();
            }            
        }
        //player is releases up/down key
        if (Input.GetButtonUp("Vertical"))
        {
            isClimbing = false;
        }
        if (hasLanded && Input.GetButtonDown("Jump"))    //player presses spacebar
        {
            hasLanded = false;
            Jump();
        }
        if (Input.GetButtonDown("Attack1")) //player presses attack button
        {
            Attack();
        }
        //test for player has landed
        if (hasLanded)
        {
            playerAnimation.Jump(false);    //stop landing animation
        }
        if(!isAttacking)
        {
            playerAnimation.Attack(false);  //stop attack animation
        }
    }

    // Walk is called when the user presses right or left
    void Walk()
    {
        float h = Input.GetAxisRaw("Horizontal");   //determines right or left movement
        //if the player is facing right but moving left
        if (facingRight && h < 0)
        {
            Flip();
        }
        else if (!facingRight && h > 0) //if the player is not facing right while moving right
        {
            Flip();
        }

        //start walk animation
        playerAnimation.Walk(h);
        //move the player sprite
        playerBody.velocity = new Vector2(h * walkSpeed, playerBody.velocity.y);
    }

    //Jump is called when the player presses the spacebar
    void Jump()
    {
        hasLanded = false;
        playerBody.velocity = Vector2.up * jumpForce;
        playerAnimation.Jump(true);
        AudioManager.instance.PlaySound("playerJump");
    }

    //Climb is called when the player presses up or down
    void Climb()
    {
        float v = Input.GetAxisRaw("Vertical");   //determines up or down movement

        hasLanded = false;  //player can't jump
        isClimbing = true;

        //stop platform collisions so player can pass through
        platformEffector.useColliderMask = false;
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Platform"));

        playerAnimation.Climb(v);   //play climbing animation

        playerBody.velocity = new Vector2(playerBody.velocity.x, v * climbSpeed);   //move player
        //change gravity so the player doesn't fall down the ladder when key is released
        playerBody.gravityScale = 0;
    }

    //Attack is called when the player presses 'j'
    void Attack()
    {
        isAttacking = true;
        playerAnimation.Attack(isAttacking);    //play animation
        AudioManager.instance.PlaySound("attack");  //play sound

        //if the enemy is in front of the player
        LayerMask layerMask = LayerMask.GetMask("Enemy");
        float originOffset = 0.35f;
        float distance = 1.0f;
        Vector2 direction = Vector2.right;

        //adjust for facing left
        if(!facingRight)
        {
            originOffset = -0.35f;
            direction = Vector2.left;
        }

        Vector2 position = new Vector2(transform.position.x + originOffset, transform.position.y);

        //Debug.DrawRay(position, direction, Color.green);    for debugging
        RaycastHit2D hit = Physics2D.Raycast(position, direction, distance, layerMask);

        //if the Raycast detects an enemy
        if (hit.collider != null)
        {            
            Enemy enemy = hit.collider.gameObject.GetComponent<Enemy>();
            if(enemy != null)
            {
                GameManager.instance.KillEnemy(enemy);
            }            
        }
    }

    //Flip is called when the player presses left/right keys
    void Flip()
    {
        // Switch the way the player is labelled as facing
        facingRight = !facingRight;

        // Multiply the player's x local scale by -1
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    //test for staying inside 2D trigger collisions
    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Ladder")
        {
            canClimb = true;
        }
    }

    //test for exiting out of 2D trigger collisions
    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Ladder")
        {
            canClimb = false;
            StopClimbing();
        }
    }

    //check for colliders touching
    void OnCollisionEnter2D(Collision2D collision)
    {
        //if player is touching platform
        if (collision.gameObject.tag == "Platform" || collision.gameObject.tag == "Spikes")
        {
            hasLanded = true;
        }
    }

    //CheckLadderBelow tracks if a ladder is below player's feet
    void CheckLadderBelow()
    {
        LayerMask ladderMask = LayerMask.GetMask("Ladder");
        float originOffset = 1.0f;
        float distance = 0.5f;
        Vector2 position = new Vector2(transform.position.x, transform.position.y - originOffset);
        Vector2 direction = Vector2.down;

        //Debug.DrawRay(position, direction, Color.green);    for debugging
        RaycastHit2D hit = Physics2D.Raycast(position, direction, distance, ladderMask);

        //if the Raycast detects a ladder below the player
        if (hit.collider != null)
        {
            ladderBelow = true;
        }
        else
        {
            ladderBelow = false;
        }
    }

    //StopsClimbing is called when the user is no longer climbing the ladder
    void StopClimbing()
    {
        isClimbing = false;
        hasLanded = true;
        playerAnimation.Climb(0);   //stop climbing animation

        //enable collisions again
        platformEffector.useColliderMask = true;
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Platform"), false);
        
        //reset gravity
        playerBody.gravityScale = gravityScaleAtStart;
    }

    //WalkSound is called separately through the PlayerWalk animation
    void WalkSound()
    {
        AudioManager.instance.PlaySound("walk");    //play walking sound
    }

    //ClimbSound is called separately through the PlayerClimb animation
    void ClimbSound()
    {
        AudioManager.instance.PlaySound("climbLadder");        //play climbing sound
    }
}
